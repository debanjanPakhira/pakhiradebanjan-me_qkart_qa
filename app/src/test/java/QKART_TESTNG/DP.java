package QKART_TESTNG;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
// import java.util.ArrayList;
// import java.util.Iterator;
// import java.util.List;
// import com.google.common.collect.Table.Cell;
// import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;


public class DP {
    @DataProvider (name = "data-provider")
    public Object[][] dpMethod (Method m) throws IOException{
        String filePath = "/home/crio-user/workspace/pakhiradebanjan-ME_QKART_QA/app/src/test/resources/Dataset.xlsx";
        File fileName = new File(filePath);
		FileInputStream file = new FileInputStream(fileName);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        
        switch(m.getName()){
            case "TestCase01" :
            XSSFSheet sheet1 = workbook.getSheet("TestCase01");
            XSSFRow row11 = sheet1.getRow(1);
            XSSFRow row12 = sheet1.getRow(2);
            XSSFRow row13 = sheet1.getRow(3);
            return new Object[][] {
            {row11.getCell(1).getStringCellValue(), row11.getCell(2).getStringCellValue()}, 
            {row12.getCell(1).getStringCellValue(), row12.getCell(2).getStringCellValue()}, 
            {row13.getCell(1).getStringCellValue(),row13.getCell(2).getStringCellValue()}
            };


            case "TestCase02" :
            XSSFSheet sheet2 = workbook.getSheet("TestCase02");
            XSSFRow row21 = sheet2.getRow(1);
            XSSFRow row22 = sheet2.getRow(2);
            XSSFRow row23 = sheet2.getRow(3);
            return new Object[][] {
            {row21.getCell(1).getStringCellValue(), row21.getCell(2).getStringCellValue()}, 
            {row22.getCell(1).getStringCellValue(), row22.getCell(2).getStringCellValue()}, 
            {row23.getCell(1).getStringCellValue(),row23.getCell(2).getStringCellValue()}
            };


            case "TestCase03" :
            XSSFSheet sheet3 = workbook.getSheet("TestCase03");
            XSSFRow row31 = sheet3.getRow(1);
            XSSFRow row32 = sheet3.getRow(2);
            XSSFRow row33 = sheet3.getRow(3);
            return new Object[][] {{row31.getCell(1).getStringCellValue()}, 
                                   {row32.getCell(1).getStringCellValue()}, 
                                   {row33.getCell(1).getStringCellValue()}};


            case "TestCase04" :
            XSSFSheet sheet4 = workbook.getSheet("TestCase04");
            XSSFRow row41 = sheet4.getRow(1);
            XSSFRow row42 = sheet4.getRow(2);
            XSSFRow row43 = sheet4.getRow(3);
            return new Object[][] {{row41.getCell(1).getStringCellValue()}, 
                                   {row42.getCell(1).getStringCellValue()}, 
                                   {row43.getCell(1).getStringCellValue()}};


            case "TestCase05" :
            XSSFSheet sheet5 = workbook.getSheet("TestCase05");
            XSSFRow row51 = sheet5.getRow(1);
            XSSFRow row52 = sheet5.getRow(2);
            XSSFRow row53 = sheet5.getRow(3);
            return new Object[][] {
            {row51.getCell(1).getStringCellValue(), row51.getCell(2).getStringCellValue(), row51.getCell(3).getStringCellValue()}, 
            {row52.getCell(1).getStringCellValue(), row52.getCell(2).getStringCellValue(), row52.getCell(3).getStringCellValue()}, 
            {row53.getCell(1).getStringCellValue(), row53.getCell(2).getStringCellValue(), row53.getCell(3).getStringCellValue()}
            };


            case "TestCase06" :
            XSSFSheet sheet6 = workbook.getSheet("TestCase06");
            XSSFRow row61 = sheet6.getRow(1);
            XSSFRow row62 = sheet6.getRow(2);
            XSSFRow row63 = sheet6.getRow(3);
            return new Object[][] {
                {row61.getCell(1).getStringCellValue(), row61.getCell(2).getStringCellValue()}, 
                {row62.getCell(1).getStringCellValue(), row62.getCell(2).getStringCellValue()}, 
                {row63.getCell(1).getStringCellValue(), row63.getCell(2).getStringCellValue()}
                };


        


    case "TestCase07" :
    XSSFSheet sheet7 = workbook.getSheet("TestCase07");
    XSSFRow row71 = sheet7.getRow(1);
    XSSFRow row72 = sheet7.getRow(2);
    XSSFRow row73 = sheet7.getRow(3);
    return new Object[][] {
    {row71.getCell(1).getStringCellValue()},
    {row72.getCell(1).getStringCellValue()},
    {row73.getCell(1).getStringCellValue()}
    };
 

    case "TestCase08" :
    XSSFSheet sheet8 = workbook.getSheet("TestCase08");
    XSSFRow row81 = sheet8.getRow(1);
    XSSFRow row82 = sheet8.getRow(2);
    XSSFRow row83 = sheet8.getRow(3);
    return new Object[][] {
    {row81.getCell(1).getStringCellValue(), (int)(row81.getCell(2).getNumericCellValue())}, 
    {row82.getCell(1).getStringCellValue(), (int)row82.getCell(2).getNumericCellValue()}, 
    {row83.getCell(1).getStringCellValue(), (int)row83.getCell(2).getNumericCellValue()}
    };

    case "TestCase11" :
            XSSFSheet sheet11 = workbook.getSheet("TestCase11");
            XSSFRow row111 = sheet11.getRow(1);
            XSSFRow row112 = sheet11.getRow(2);
            XSSFRow row113 = sheet11.getRow(3);
            return new Object[][] {
            {row111.getCell(1).getStringCellValue(), row111.getCell(2).getStringCellValue(), row111.getCell(3).getStringCellValue()}, 
            {row112.getCell(1).getStringCellValue(), row112.getCell(2).getStringCellValue(), row112.getCell(3).getStringCellValue()}, 
            {row113.getCell(1).getStringCellValue(), row113.getCell(2).getStringCellValue(), row113.getCell(3).getStringCellValue()}
            };


            case "TestCase12" :
            XSSFSheet sheet12 = workbook.getSheet("TestCase12");
            XSSFRow row121 = sheet12.getRow(1);
            XSSFRow row122 = sheet12.getRow(2);
            XSSFRow row123 = sheet12.getRow(3);
            return new Object[][] {
            {row121.getCell(1).getStringCellValue(), row121.getCell(2).getStringCellValue()}, 
            {row122.getCell(1).getStringCellValue(), row122.getCell(2).getStringCellValue()}, 
            {row123.getCell(1).getStringCellValue(), row123.getCell(2).getStringCellValue()}
            };
        }
        return null;
    }
}