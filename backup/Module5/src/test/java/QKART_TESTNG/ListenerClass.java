package QKART_TESTNG;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerClass extends QKART_Tests implements ITestListener{
    public void onTestStart(ITestResult result){
        takeScreenshot("Start_Testcase", result.getMethod().getMethodName());
    }

    public void onTestSuccess (ITestResult result){
        takeScreenshot("End_Testcase", result.getMethod().getMethodName());
    }
    public void onTestFailure(ITestResult result){
        takeScreenshot("Failed_Testcase", result.getMethod().getMethodName());
    }
}